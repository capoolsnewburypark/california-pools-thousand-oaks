California Pools is one of the largest pool builders in the country and brings award-winning custom pools to Thousand Oaks. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 107 N Reino Rd, #345, Newbury Park, CA 91320, USA

Phone: 805-495-7220

Website: https://californiapools.com/locations/thousand-oaks